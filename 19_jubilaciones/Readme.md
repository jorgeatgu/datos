Este CSV incluye el número de pensiones de jubilación (en número de personas) y la pensión media de jubilación (en euros) por cada municipio español. Cuando una celda queda vacía, singifica
que no había datos en la fuente original. Se han añadido tres campos al archivo: dos identificadores de ID y una columna para las provincias.

La información está actualizada a Octubre de 2018.  

La fuente de estos datos es una serie de respuestas parlamentarias en el Senado. El documento original (en PDF) se encuentra [aquí](http://www.senado.es/web/actividadparlamentaria/iniciativas/detalleiniciativa/textosexpedientes/index.html?legis=12&id1=684&id2=052703). 


