**Repositorio de Datos de El País**

A menos que no se especifique lo contrario, todos los datasets están disponibles bajo [Licencia Creative Commons - Attribution 4.0](https://creativecommons.org/licenses/by/4.0/). 
Si la información te resulta útil o tienes alguna duda, [puedes ponerte en contacto con nosotros](mailto:dgrasso@elpais.es).