Resultados de las elecciones generales del 28 de abril de 2019 en cada municipio español. 
Las columnas incluyen:

*  código de municipio
*  ccaa
*  provincia
*  nombre del municipio
*  censo electoral
*  porcentaje escrutado (siempre = 100)
*  número de votantes
*  porcentaje de participación
*  votos validos
*  votos blancos
*  votos nulos
*  Columnas con el porcentaje de voto a cada partido

En todos los campos se utiliza el punto (.) como separador de decimales. 